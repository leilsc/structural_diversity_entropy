# structural_diversity_entropy

This repository accompanies the manuscript with the title: 'Structural diversity entropy: a unified diversity measure to detect latent landscape features'. 

The R scripts must be run in the order as indicated in the methods folder.

Please install the R package StrucDivDevel from the provided .tar file. This is the version that was used to generate the results presented in the manuscript. A newer version will be submitted to CRAN.

