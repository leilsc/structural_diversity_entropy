#=================================================================================================
# Structural diversity of NDVI data
#=================================================================================================
# This is the template script for structural diversity estimation in NDVI data, using 
# window side length (WSL) = 3
# For all other WLS ∈ W1={3, 5, 7, 9, 11, 13, 15, 17, 19, 25, 35, 45}, please replace arguments 
# "NgbProb" and "NgbHet" with the respective WSL, and adapt file names.
# For all binned data, please load the respective file instead, and adapt file names.
# All model settings remain the same
# Metrics tested: entropy, weighted entropies, contrast, dissimilarity, homogeneity
# "Spectral" means "value", and "Position" means "rank" in the function settings.
# Distance ("Dist") 1, all directions ("angle") specifies the spatial relationship of pixels.
# Please use the .tar.gz file to install the R package StrucDivDevel.
# If any NAs occur in the window, NA is returned ("na.rm = FALSE").
#=================================================================================================

library(raster)
library(StructDivDevel)

#=================================================================================================

dir.create("Methods/StructuralDiversity/tif")

#=================================================================================================

nee <- raster("Data/NDVI.NAfill.2018.nee.tif")

#=================================================================================================
# WSL 3
#=================================================================================================

# Entropy
nee.Ent.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "Entropy", 
                          Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)

# Weighted Entropy
nee.AbsValEnt.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "WeightedEntropyAbsSpectral", 
                                Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)
nee.SqrValEnt.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "WeightedEntropySqrSpectral", 
                                Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)
nee.AbsRankEnt.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "WeightedEntropyAbsPosition", 
                                 Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)
nee.SqrRankEnt.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "WeightedEntropySqrPosition", 
                                 Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)

# Contrast
nee.ValCon.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "ContrastSpectral", 
                                 Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)
nee.RankCon.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "ContrastPosition", 
                             Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)
# Dissimilarity
nee.ValDis.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "DissimilaritySpectral", 
                             Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)
nee.RankDis.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "DissimilarityPosition", 
                              Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)

# Homogeneity
nee.Hom.3 <- FocalRcppNew(x = nee, NgbProb = 3, NgbHet = 3, fun = "HomogeneityPosition", 
                              Dist = 1, na.rm = FALSE, angle = "all", parallelize = TRUE)

#=================================================================================================

writeRaster(nee.Ent.3, "Methods/StructuralDiversity/tif/nee.Ent.3.tif")
writeRaster(nee.AbsValEnt.3, "Methods/StructuralDiversity/tif/nee.AbsValEnt.3.tif")
writeRaster(nee.SqrValEnt.3, "Methods/StructuralDiversity/tif/nee.SqrValEnt.3.tif")
writeRaster(nee.AbsRankEnt.3, "Methods/StructuralDiversity/tif/nee.AbsRankEnt.3.tif")
writeRaster(nee.SqrRankEnt.3, "Methods/StructuralDiversity/tif/nee.SqrRankEnt.3.tif")
writeRaster(nee.RankCon.3, "Methods/StructuralDiversity/tif/nee.RankCon.3.tif")
writeRaster(nee.ValDis.3, "Methods/StructuralDiversity/tif/nee.ValDis.3.tif")
writeRaster(nee.RankDis.3, "Methods/StructuralDiversity/tif/nee.RankDis.3.tif")
writeRaster(nee.Hom.3, "Methods/StructuralDiversity/tif/nee.Hom.3.tif")

#=================================================================================================

